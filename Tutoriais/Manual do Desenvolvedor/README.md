# Manual do Desenvolvedor

## Histórico de Alterações

**Data** | **Descrição** | **Autor**
:---:|:---------:|:-------:
15/09/18 | Inicio do Manual | Victor Murilo
18/09/18 | Inserindo Mensagens Suportadas/Não-Suportadas | Victor Murilo
23/09/18 | Modificação via Jupyter - Inserindo novo diagrama| Victor Murilo

## Sumário

* [Introdução] (https://gitlab.com/ad-si-2018-2/p1-g2/tree/master/Tutoriais/Manual%20do%20Desenvolvedor/README.md#1-introdu%C3%A7%C3%A3o)
    * [Público Alvo] (https://gitlab.com/ad-si-2018-2/p1-g2/tree/master/Tutoriais/Manual%20do%20Desenvolvedor/README.md#11-p%C3%BAblico-alvo)
    * [Finalidade] (https://gitlab.com/ad-si-2018-2/p1-g2/tree/master/Tutoriais/Manual%20do%20Desenvolvedor/README.md#12-finalidade)
* [Mensagens Suportadas] (https://gitlab.com/ad-si-2018-2/p1-g2/tree/master/Tutoriais/Manual%20do%20Desenvolvedor/README.md#2-mensagens-suportadas)
* [Mensagens Não Suportadas] (https://gitlab.com/ad-si-2018-2/p1-g2/tree/master/Tutoriais/Manual%20do%20Desenvolvedor/README.md#3-mensagens-n%C3%A3o-suportadas)
* [Diagramas] (https://gitlab.com/ad-si-2018-2/p1-g2/tree/master/Tutoriais/Manual%20do%20Desenvolvedor/README.md#4-diagramas)
* [Referências e Materiais Externos] (https://gitlab.com/ad-si-2018-2/p1-g2/tree/master/Tutoriais/Manual%20do%20Desenvolvedor/README.md#5-referencias-e-materiais-externos)

***

### 1. Introdução

#### 1.1. Público Alvo
<p>As informações deste manual são destinadas aos desenvolvedores de software, analistas programadores e 
público em geral interessado em aprender como se utilizar este material de uso do IRC.
Apesar das mensagens suportadas estarem listadas no manual de usuário, aqui estão listadas em conjunto com as não suportadas para fins de instrução aos desenvolvedores em seu trabalho.
</p>

#### 1.2. Finalidade
<p>Os usuários do canal acessam, podem conversar e sair do canal.
As mensagens enviadas dentro do canal serão replicadas para todos os usuários deste mesmo âmbito.
</p>
***

### 2. Mensagens Suportadas

* Join (/join #Canal)
    * Comando utilizado para acessar um determinado canal.
* Part (/part #canal [mensagem])
    * Faz sair do canal determinado (a mensagem é opcional).
* Mode (/mode #canal | nick [[+ | - ] modos [parametros]])
    * Muda o modo de canais ou usuários.
* Names (/names #canal)
    * Este comando lista os nomes ativos no canal.
* Topic (/topic #canal novotopic)
    * Insere um tópico no canal ou modifica um pré-existente.
* List (/list [#string])
    * Este comando lista os canais (Você pode pegar apenas os que têm um trecho(string) no seu nome).
* Invite (/invite nick #canal)
    * Este comando insere o usuário "nick" no canal declarado.
* Kick (/kick #canal nick)
    * Remove o usuário "nick"

***

### 3. Mensagens não Suportadas

* Partall
* Notice e seus derivados
* Action e seus derivados
* Query
* Who e seus derivados
* Away
* Disconnect

***

### 4. Diagramas

* Diagramas de Atividades:
    * 1:
![Alt Text](https://uploaddeimagens.com.br/images/001/623/916/original/Sem_t%C3%ADtulo.png?1537415229)
    * 2:
![Alt Text](https://uploaddeimagens.com.br/images/001/631/284/original/2222.png?1537742882)

***

### 5. Referencias e Materiais Externos

* <a href="https://www.cielo.com.br/wps/wcm/connect/c682298e-4518-4e2b-8945-cef23e04b5ec/Cielo-E-commerce-Manual-do-Desenvolvedor-WebService-PT-V2.5.4.pdf?MOD=AJPERES&CONVERT_TO=url?utm_source=SECNET&utm_campaign=Checkout%20Transparente%20e%20Certificado%20SS">Manual do Desenvolvedor - CIELO</a>
* <a href="https://www.lucidchart.com">Lucid chart - Criação de Diagramas</a>

***