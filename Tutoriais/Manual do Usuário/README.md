# Manual do Usuário

## Histórico de Alterações

**Data** | **Descrição** | **Autor**
:---:|:---------:|:-------:
15/09/18 | Inicio do Manual | Victor Murilo
23/09/18 | Modificação via Jupyter| Victor Murilo

## Sumário

* [Introdução] (https://gitlab.com/ad-si-2018-2/p1-g2/tree/master/Manual%20do%20Usuário%20/#1-introdu%C3%A7%C3%A3o)
* [Mensagens Suportadas] (https://gitlab.com/ad-si-2018-2/p1-g2/tree/master/Manual%20do%20Usuário%20/#2-mensagens-suportadas)
* [Testes Realizados] (https://gitlab.com/ad-si-2018-2/p1-g2/tree/master/Manual%20do%20Usuário%20/#3-testes-realizados)
* [Referências e Materiais Externos] (https://gitlab.com/ad-si-2018-2/p1-g2/tree/master/Manual%20do%20Usuário%20/#4-referencias-e-materiais-externo)

***

### 1. Introdução

#### 1.1. Público Alvo
<p>As informações deste manual são destinadas aos usuários do do IRC. 
</p>

***

### 2. Mensagens Suportadas

* Join (/join #Canal)
    * Comando utilizado para acessar um determinado canal.
* Part (/part #canal [mensagem])
    * Faz sair do canal determinado (a mensagem é opcional).
* Mode (/mode #canal | nick [[+ | - ] modos [parametros]])
    * Muda o modo de canais ou usuários.
* Names (/names #canal)
    * Este comando lista os nomes ativos no canal.
* Topic (/topic #canal novotopic)
    * Insere um tópico no canal ou modifica um pré-existente.
* List (/list [#string])
    * Este comando lista os canais (Você pode pegar apenas os que têm um trecho(string) no seu nome).
* Invite (/invite nick #canal)
    * Este comando insere o usuário "nick" no canal declarado.
* Kick (/kick #canal nick)
    * Remove o usuário "nick"

***

### 3. Testes Realizados

***

### 4. Referencias e Materiais Externos

***