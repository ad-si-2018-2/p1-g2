        // Definindo bibliotecas, portas e demais classes

        port = 4000;
        net = require('net');

        var clients = []; //vetor de  clientes
        var nick = {}; //nicks
        var channels = ['#Canal-g2'];

        //Cria o servidor
          net.createServer(function (socket) {
          socket.name = socket.remoteAddress + ":" + socket.remotePort; //É dado um nome, com base no IP remoto e na porta que fez a requisição
          socket.channel = '';// É um canal vazio ''
          clients.push(socket);//O objeto é adicionado na lista de clientes
          var inicioTempo = dataString();// Variável que conta o tempo do servidor.

          //Mostra mensagem de conexão na tela
           var msgConect = "Conectando...\nVocê está conectado!\n";
           socket.write(msgConect);

          //Lidar com mensagens recebidas de clientes.
           socket.on('dados', function(data) {
           onDataReceived(String(data));
           });

          //Remove o cliente da lista quando sair
           socket.on('fim', function() {
           onCloseConnection();
            });
            //Fecha a conexão
             function onCloseConnection() {
                   connection.quit(server, user, undefined);
               }

          //Valida o comando
          socket.on('data', function (data) {

          var comando = data.toString("utf-8").trim().split(' '); //variavel que recebe a string e transforma em vetor de pequenas strings
          var cmd = comando[0].toUpperCase();

           switch (cmd) {
                  //Inclui novo usuário para um canal
                  case "JOIN":
                      try {
                          if (channels.indexOf(comando[1]) >= 0) {
                              socket.channel = comando[1].trim();
                              socket.write("Você está no canal: " + comando[1] + "\n");
                              broadcast(socket.name.split("|")[1] + " entrou no chat.\n", socket);
                          }
                          else {
                              throw 475;
                          }
                      }catch(err) {
                          socket.write(erros(err));}
                      finally {
                          break;
                      }

                  //Mostra a lista de canais no servidor.
                  case "LIST":
                      try{
                          if(!comando[1]) {
                                  var channel = '';
                                  for (i = 0; i < channels.length; i++) {
                                      channel += channels[i] + "\n";
                                      }
                                  socket.write(channel);
                               }
                          else
                               throw 422;

                      }catch(err) {
                          socket.write(erros(err));}
                      finally {
                          break;
                      }

                  //Altera Nick
                  case "NICK":
                      try {
                          //Busca o <nickname> pela chave no vetor
                          if (typeof(nick[comando[1]]) != "undefined")
                              throw 433;
                          else {
                              if (!comando[1])
                                  throw 432;
                              else {
                                  delete nick[socket.name.split("//")[1]];

                                  //Realiza associação do Nick ao Endereço IP no vetor
                                  nick[comando[1]] = socket.remoteAddress;
                                  socket.name = socket.remoteAddress + ":" + socket.remotePort;
                                  socket.name += "//" + comando[1];
                                  socket.write("Alteração do Nick\n");
                              }
                          }
                      }catch(err) {
                          socket.write(erros(err));
                      }
                      finally {
                              break;
                          }

                  //Manda msg privada. (Comando para enviar: PRIVMSG USER :MESSAGE)
                  case "PRIVMSG":

                      try {
                          // A Variável que recebe o nome de destinatario':'
                          var to = data.toString("utf-8").split(" ")[1];

                          /* A Variável msg recebe texto digitado depois de ':'*/
                          var msg = data.toString("utf-8").split(':')[1];
                          if (!to)
                              throw 461;
                          else if (!msg)
                              throw 411;
                          else
                              //Envia a mensagem se não detectar erros
                              sendPriv(socket, socket.name.split("//")[1] + ".....| " + msg, to);
                          }
                      catch(err) {
                          socket.write(erros(err));
                      }
                      finally {
                          break;
                      }
                  //Desconectar. Apaga o usuário do vetor.
                  case "QUIT":
                      try {
                          if(!comando[1]) {
                              var nickname = socket.name.split("|")[1];
                              delete nick[socket.name.split("|")[1]];
                              clients.splice(clients.indexOf(socket), 1);
                              socket.destroy();
                              broadcast(nickname + " Saiu do Chat.\n");

                          }
                          else
                              throw 422;
                      }
                      catch(err) {
                          socket.write(erros(err));
                      }
                      finally {

                          break;
                      }

                  //Recebe nicknames de usuários conectados.
                  case "USERS":
                    try {
                        if(!comando[1]) {
                                var users = "";
                                for (var i in nick) {
                                    users += i + "\n";
                                }
                                socket.write(users);
                          }
                        else
                            throw 422;
                    }catch(err) {
                        socket.write(erros(err));}
                    finally {
                        break;
                    }

                  //Caso nao ser um comando envia para todos uma mensagem
                  default:
                        broadcastchannel(socket.name.split("|")[1] + " ---> " + data.toString("utf-8"), socket);
                        break;

              }
              //Remove o cliente quando encerra a conexao
                socket.on('end', function () {
                  clients.splice(clients.indexOf(socket), 1);

              //Envia mensagem privada
                function sendPriv(sender, message, user) {
                    clients.forEach(function (client) {
                        var channel = sender.channel;
                        if(client.channel == '') return;
                        if ((client.channel == channel) && (client.name.split("|")[1] == user)) client.write(message);
                    });
                    process.stdout.write(message)
                }

                 //Envia mensagem para todos, menos para quem enviou a mensagem
                  function broadcastchannel(message, sender) {
                      clients.forEach(function (client) {
                          var channel = sender.channel;
                          if(client.channel == '') return;
                          if (client.channel == channel) client.write(message);
                      });
                      process.stdout.write(message)
                  }
                  
            });listen(server.port);

               // Mensagem que aparece no servidor
               console.log("Servidor do chat disponivel na porta 4000");
        
    

    
